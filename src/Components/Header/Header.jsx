import React from 'react'
import { Desktop, Mobile, Tablet } from '../../Layout/Responsive'
import HeaderDesktop from './HeaderDesktop'
import HeaderMobile from './HeaderMobile'
import HeaderTablet from './HeaderTablet'
import HeaderTables from './HeaderTablet'

export default function Header() {
    return (
        <div>
                <HeaderDesktop />
            {/* <Desktop>
            </Desktop>
            <Tablet>
                <HeaderTablet />
                <HeaderDesktop />
            </Tablet>
            <Mobile>
                <HeaderMobile />
            </Mobile> */}
        </div>
    )
}
