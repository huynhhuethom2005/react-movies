import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink, useNavigate } from 'react-router-dom';
import { localUserSer } from '../../service/localService';
import UserDropdown from './UserDropdown';

export default function UserMenu() {
    let userInfor = useSelector((state) => {
        return state.userReducer.userInfor
    })
    console.log('userInfor :>> ', userInfor);
    let navigate = useNavigate()

    let handleLogout = () => {
        localUserSer.remove()
        // window.location.href = "/login-page"
        navigate("/login-page")
    }

    let buttonCss = "mx-2 px-5 py-2 border-2 border-black"
    let renderContent = () => {
        if (userInfor) {
            return (<>
                {/* <span>{userInfor.hoTen}</span> */}
                <UserDropdown userName={userInfor} />
                <button onClick={handleLogout} className={buttonCss}>Dang xuat</button>
            </>)
        } else {
            return (<>
                <NavLink to="/login-page"><button className={buttonCss}>Dang nhap</button></NavLink>
                <button className={buttonCss}>Dang ky</button>
            </>)
        }
    }

    return (
        <div className='space-x-5 flex items-center'>{renderContent()}</div>
    )
}
