import { Progress } from 'antd'
import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieService } from '../../service/movieService'

export default function DetailPage() {
    let { id } = useParams()
    let [movie, setMovie] = useState({})

    useEffect(() => {
        /* async await ~ then catch */
        let fetchDetail = async () => {
            try {
                let result = await movieService.getDetailMovie(id)
                setMovie(result.data.content)
            } catch (error) {
                console.log('error :>> ', error);
            }
        }
        fetchDetail()
    }, [])

    return (
        <div className='container'>
            <div className="flex space-x-10">
                <img src={movie.hinhAnh} alt="" className='w-1/3' />
                <div className="">
                    <h1 className='font-medium'>{movie.tenPhim}</h1>
                    <div>{movie.moTa}</div>
                    <Progress percent={movie.danhGia * 10} />
                </div>
            </div>
            <NavLink className="rounded px-5 py-2 bg-red-600 text-white font-medium" to={`/booking/${id}`}>Mua ve</NavLink>
            <button className="rounded px-5 py-2 bg-red-600 text-white font-medium">OK</button>
        </div>
    )
}
