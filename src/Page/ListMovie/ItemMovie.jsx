import React from 'react'
import { Card } from 'antd';
import { NavLink } from 'react-router-dom';
const { Meta } = Card;

export default function ItemMovie({ data }) {
    return (
        <Card
            hoverable cover={<img className='h-40 object-cover object-top' alt="example" src={data.hinhAnh} />}
        >
            <Meta className='h-20' title={data.tenPhim} description={<NavLink to={`/detail/${data.maPhim}`} className={"px-5 py-2 border-2 border-red-200 rounded"}>View movie</NavLink>} />
        </Card>
    )
}
