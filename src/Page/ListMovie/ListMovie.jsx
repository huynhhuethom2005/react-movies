import React, { useEffect, useState } from 'react'
import { movieService } from '../../service/movieService'
import ItemMovie from './ItemMovie';

export default function ListMovie() {
    const [movies, setMovies] = useState([])

    useEffect(() => {
        movieService.movieList().then((result) => {
            setMovies(result.data.content)
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }, [])

    return (
        <div className='container grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-10'>
            {movies.map((item) => {
                return <ItemMovie data={item} key={item.maPhim} />
            })}
        </div>
    )
}
