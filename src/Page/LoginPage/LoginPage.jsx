import React from 'react'
import { Button, Form, Input, message } from 'antd';
import { userService } from '../../service/userService';
import { localUserSer } from '../../service/localService';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import Lottie from "lottie-react";
import login_animate from "../../assets/login_animate.json"
import { setLoginAction, setLoginActionService } from '../../redux/action/userAction';

export default function LoginPage() {
    let navigate = useNavigate()
    let dispatch = useDispatch()
    /* FIXME: onFinish with desux */
    const onFinish = (values) => {
        console.log('Success:', values);
        userService.postLogin(values).then((result) => {
            console.log('result :>> ', result);
            message.success("Login thanh cong!")
            localUserSer.set(result.data.content) // Save user infor localStorge
            navigate("/") // Router to HomePage
            dispatch(setLoginAction(result.data.content))
        }).catch((err) => {
            console.log('err :>> ', err);
            message.error("Login that bai!")
        });
    };

    /* FIXME: onFinish with redux-thunk */
    const onFinishThunk = (value) => {
        let onSuccess = () => {
            message.success("Login thanh cong!")
            navigate("/") // Router to HomePage
        }
        dispatch(setLoginActionService(value, onSuccess))
    }

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className="h-screen w-screen flex bg-orange-200 justify-center items-center">
            <div className="container mx-auto p-5 bg-white rounded flex">
                <div className="w-1/2 h-full">
                    <Lottie animationData={login_animate} loop={true} />
                </div>
                <div className="w-1/2 h-full">
                    <Form
                        name="basic"
                        labelCol={{
                            span: 8,
                        }}
                        wrapperCol={{
                            span: 24,
                        }}
                        style={{
                            width: "100%"
                        }}
                        initialValues={{
                            remember: true,
                        }}
                        onFinish={onFinishThunk}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                        layout='vertical'
                    >
                        <Form.Item
                            label="Username"
                            name="taiKhoan"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!',
                                },
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Password"
                            name="matKhau"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!',
                                },
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                        <Form.Item
                            wrapperCol={{
                                span: 24,
                            }}
                            className='flex justify-center items-center'
                        >
                            <Button className='bg-blue-600' type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>

            </div>
        </div>
    )
}
