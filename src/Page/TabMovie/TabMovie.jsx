import React, { useEffect, useState } from 'react'
import { movieService } from '../../service/movieService';
import { Tabs } from 'antd';
import ItemTabMovie from './ItemTabMovie';
const onChange = (key) => {
    console.log(key);
};
const items = [
    {
        key: '1',
        label: `Tab 1`,
        children: `Content of Tab Pane 1`,
    }
];

export default function TabMovie() {
    let [hethongRap, setHethongRap] = useState([])

    useEffect(() => {
        movieService.getMovieByTheater().then((result) => {
            setHethongRap(result.data.content)
            console.log('result :>> ', result);
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }, [])

    let renderHethongRap = () => {
        return hethongRap.map((rap) => {
            return {
                key: rap.maHeThongRap,
                label: <img className='h-16' src={rap.logo} alt="" />,
                children: <Tabs style={{ height: 500 }} tabPosition='left' defaultActiveKey="1" items={rap.lstCumRap.map((cumRap) => {
                    return {
                        key: cumRap.tenCumRap,
                        label: <div>{cumRap.tenCumRap}</div>,
                        children: <div className='overflow-y-scroll' style={{ height: 500 }}>{cumRap.danhSachPhim.map((item) => {
                            return <ItemTabMovie phim={item} />
                        })}</div>,
                    }
                })} onChange={onChange} />,
            }
        })
    }

    return (
        <div className="container">
            <Tabs style={{ height: 500 }} tabPosition='left' defaultActiveKey="1" items={renderHethongRap()} onChange={onChange} />
        </div>
    )
}
