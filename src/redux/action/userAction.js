import { localUserSer } from "../../service/localService";
import { userService } from "../../service/userService";
import { USER_LOGIN } from "../constant/userConstant";

export const setLoginAction = (payload) => ({
    // FIXME: payload from axios result
    type: USER_LOGIN,
    payload: payload
})

export const setLoginActionService = (payload, onComplated) => {
    // FIXME: payload from value of form antd
    return (dispatch) => {
        userService.postLogin(payload).then((result) => {
            console.log('result :>> ', result);
            onComplated()
            localUserSer.set(result.data.content) // Save user infor localStorge
            dispatch(setLoginAction(result.data.content))
        }).catch((err) => {
            console.log('err :>> ', err);
        });
    }
}

