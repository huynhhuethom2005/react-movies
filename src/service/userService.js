import { BASE_URL, configHeaders, https } from "./config"

export const userService = {
    postLogin: (loginForm) => {
        /* With post method use post() */
        return https.post("/api/QuanLyNguoiDung/DangNhap" /* API */, loginForm /* data */)
    }
}
