import { https } from "./config"

export const movieService = {
    movieList: () => {
        return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP00")
    },

    getMovieByTheater: () => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap")
    },

    getDetailMovie: (maPhim) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`)
    }
}
